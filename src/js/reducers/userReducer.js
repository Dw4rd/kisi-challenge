export default function reducer(state={
	locks: [],
	unlockingIds: [],
	
	fetching_locks: false,
	fetched_locks: false,
	unlocking_lock: false,
	unlocked_lock: false,

	error: null,
}, action){
	switch(action.type){
		case "FETCHING_LOCKS": {
			return {
				...state, 
				fetching_locks: true
			}
		}
		case "FETCH_LOCKS_SUCCESSFUL": {
			return {
				...state,
				fetching_locks: false,
				fetched_locks: true,
				locks: action.payload,
			}
		}
		case "FETCH_LOCKS_FAILED": {
			return {
				...state, 
				fetching_locks: false, 
				fetched_locks: false, 
				error: action.payload
			}
		}
		case "UNLOCKING_LOCK": {
			return {
				...state,
				unlocking_lock: true,
				unlockingIds: [...state.unlockingIds, action.payload]
			}
		}
		case "UNLOCK_SUCCESSFUL": {
			const newUnlockingIds = [];
			return {
				...state, 
				unlocking_lock: false, 
				unlocked_lock: true,
				unlockingIds: newUnlockingIds
			}
		}
		case "UNLOCK_FAILED": {
			return {
				...state,
				unlocking_lock: false, 
				unlocked_lock: false,
				error: action.payload
			}
		}
		case "UNLOCK_FAILED_UNSET": {
			const newUnlockingIds = [];
			return {
				...state, 
				unlockingIds: newUnlockingIds
			}
		}
	}
	return state
}