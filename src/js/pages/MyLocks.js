import React from "react";
import Divider from "material-ui/Divider";
import CircularProgress from 'material-ui/CircularProgress';
import { connect } from "react-redux";


import { getLocks, unlockDoor } from "../actions/userActions";

import LockList from "../components/LockList";

@connect((store) => {
	return {
		locks: store.user.locks,
		unlockingIds: store.user.unlockingIds,
		fetchingLocks: store.user.fetching_locks,
		unlockingLock: store.user.unlocking_lock,
	};
})
export default class MyLocks extends React.Component{
		constructor(){
		super();
		this.triggerUnlock = this.triggerUnlock.bind(this);
	}
	componentWillMount(){
		this.props.dispatch(getLocks())
	}
	triggerUnlock(id){
		this.props.dispatch(unlockDoor(id))
	}

	render(){
		const { locks, unlockingIds, unlockingLock } = this.props;

		const mappedLocks = locks.map((lock,i) => <li key={i}>{lock.name}</li>);
		const styles = {
			layoutLoader:{
				position: "fixed",
				top: "43%",
				left: "45%"				
			}
		}

		const displayedLayout = this.props.fetchingLocks ? <div><CircularProgress style={styles.layoutLoader} size={130} thickness={7} /></div> : <div><LockList locks={locks} unlockingIds={unlockingIds} triggerUnlock={this.triggerUnlock} unlockingLock={unlockingLock} /><Divider /></div>;

		return(
			<span>
					{displayedLayout}
			</span>
		);
	}
}