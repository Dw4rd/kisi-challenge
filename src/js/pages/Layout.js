import React from "react";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Nav from "../components/Nav";

export default class Layout extends React.Component{

	render(){

		return (
			<MuiThemeProvider>
				<span>
					<Nav />
					{this.props.children}
				</span>
			</MuiThemeProvider>
			);
	}
}