import Kisi from "kisi-client";

const kisiClient = new Kisi();
kisiClient.setLoginSecret("94c2056abb993b570517f2d3a89c9b5a")

export function getLocks(){
	return function(dispatch){
		dispatch({type: "FETCHING_LOCKS"})
		kisiClient.get("locks")
			.then((locks) => {
				dispatch({type: "FETCH_LOCKS_SUCCESSFUL", payload: locks.data})
			})
			.catch((err) => {
				dispatch({type: "FETCH_LOCKS_FAILED", payload: err})
			})
	}
}

export function unlockDoor(id){
	return function(dispatch){
		dispatch({type: "UNLOCKING_LOCK", payload: {id}})
		kisiClient.post("locks/" + id +"/unlock", {})
			.then((result) => {
				dispatch({type: "UNLOCK_SUCCESSFUL", payload: {id}})
			})
			.catch((err) => {
				dispatch({type: "UNLOCK_FAILED", payload: err.reason})
				dispatch({type: "UNLOCK_FAILED_UNSET", payload: {id}})
			})
	}
}