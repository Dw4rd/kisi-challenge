import React from "react";
import AppBar from 'material-ui/AppBar';

export default class Nav extends React.Component{
	render(){
		const styleVar={
			nav:{
				backgroundColor: "#8BC34A",
			}
		};
		return(
		  <AppBar
		  	style={styleVar.nav}
		    title="My Locks"
		    iconClassNameRight="muidocs-icon-navigation-expand-more"
		  />
		);
	}
}