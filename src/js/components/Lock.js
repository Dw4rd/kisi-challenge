import React from "react";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card, CardHeader} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import ActionLockOpen from 'material-ui/svg-icons/action/lock-open';
import CircularProgress from 'material-ui/CircularProgress';

export default class Lock extends React.Component{
	constructor(){
		super();
		this.unlock = this.unlock.bind(this);
	}
	
	unlock(){
		this.props.triggerUnlock(this.props.id)
	}

	render(){
		const locksData = this.props;
		const unlockingData = this.props.unlockingIds

		const styles={
			icon:{
		    width: 48,
    		height: 48,
    		color: "#fff",
			},
			button:{
		    width: 96,
		    height: 96,
		    padding: 24,
		    backgroundColor: "#8BC34A",
		    borderRadius: "50%",
			},
		};
		const button = <IconButton iconStyle={styles.icon} style={styles.button} onClick={this.unlock}><ActionLockOpen /></IconButton>;
		const loadingPlaceholder = <CircularProgress size={96} thickness={5}/>

		
		let idExists = unlockingData.find(dataId => dataId.id === locksData.id);
		let displayedButton = "";
		if(locksData.unlockingLock && idExists){
			displayedButton = loadingPlaceholder;	
		} else{
			displayedButton = button;
		}

		return(
			<Card>
				<CardHeader 
					title={locksData.name}
					subtitle={"Created at:" + locksData.createdAt}
					avatar={displayedButton}
				/>
			</Card>
		);
	}
}