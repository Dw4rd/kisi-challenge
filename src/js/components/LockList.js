import React from "react";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Lock from "./Lock";

export default class LockList extends React.Component{
	constructor(){
		super();
		this.triggerUnlock = this.triggerUnlock.bind(this);
	}
	triggerUnlock(id){
		this.props.triggerUnlock(id)
	}

	render(){
		const { locks, unlockingLock, unlockingIds } = this.props;
		const listStyle = {
			listStyleType: "none"
		};

		const mappedLocks = locks.map((lock,i) => 
			<li key={i}>
				<Lock 
					triggerUnlock={this.triggerUnlock}
					unlockingLock={unlockingLock}
					unlockingIds={unlockingIds}

					name={lock.name}
					createdAt={lock.createdAt}
					id={lock.id}
				/>
			</li>);
		
		return(
			<span>
				<ul style={listStyle}>{mappedLocks}</ul>
			</span>
		);
	}
}